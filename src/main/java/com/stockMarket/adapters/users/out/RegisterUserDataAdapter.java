package com.stockMarket.adapters.users.out;

import com.stockMarket.application.port.out.RegisterUserDataPort;
import com.stockMarket.domain.user.RegisterUserException;
import com.stockMarket.domain.user.UserData;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class RegisterUserDataAdapter implements RegisterUserDataPort {

    private final UserRepository userRepository;

    @Override
    public void saveUserData(UserData userData) {
        var userOpt = userRepository.findByEmail(userData.email());
        if (userOpt.isPresent()) throw new RegisterUserException(HttpStatus.BAD_REQUEST, "The email sent is already used.");
        userRepository.save(UserEntity.builder()
                .name(userData.name())
                .lastname(userData.lastname())
                .email(userData.email())
                .build());
    }
}
