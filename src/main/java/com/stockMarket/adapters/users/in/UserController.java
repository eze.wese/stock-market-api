package com.stockMarket.adapters.users.in;

import com.stockMarket.application.port.in.RegisterUserUseCase;
import com.stockMarket.domain.user.UserData;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("user")
@RequiredArgsConstructor
public class UserController {

    private final RegisterUserUseCase registerUserUseCase;

    @PostMapping("/register")
    public ResponseEntity<Map<String, String>> registerUser(@RequestBody UserData userData) {
        return ResponseEntity.ok(Map.of("api_key", registerUserUseCase.registerUser(userData)));
    }
}
