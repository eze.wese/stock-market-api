package com.stockMarket.adapters.stockMarketData.in;

import com.stockMarket.application.port.in.RetrieveStockMarketDataUseCase;
import com.stockMarket.domain.stockMarketData.StockMarketDataResponse;
import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Refill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;

@RequestMapping("stock-market")
@RestController
public class StockMarketDataController {

    private final RetrieveStockMarketDataUseCase retrieveStockMarketDataUseCase;
    private final Bucket bucket;

    @Autowired
    public StockMarketDataController(RetrieveStockMarketDataUseCase retrieveStockMarketDataUseCase) {
        this.retrieveStockMarketDataUseCase = retrieveStockMarketDataUseCase;
        Bandwidth limit = Bandwidth.classic(20, Refill.greedy(20, Duration.ofMinutes(1)));
        this.bucket = Bucket.builder()
                .addLimit(limit)
                .build();
    }

    @GetMapping("/data/{symbol}")
    public ResponseEntity<StockMarketDataResponse> getStockMarketData(@PathVariable final String symbol, @RequestHeader("api-key") String apiKey) {
        if (bucket.tryConsume(1)) return ResponseEntity.ok(retrieveStockMarketDataUseCase.retrieveStockMarketData(symbol, apiKey));
        return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
    }
}
