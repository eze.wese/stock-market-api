package com.stockMarket.adapters.stockMarketData.out;

import com.stockMarket.apis.AlphaVantageApi;
import com.stockMarket.application.port.out.RetrieveStockMarketInfoPort;
import com.stockMarket.domain.stockMarketData.StockMarketDataResponse;
import com.stockMarket.domain.stockMarketData.StockMarketDataResponse.Price;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;

@RequiredArgsConstructor
@Component
public class RetrieveStockMarketInfoAdapter implements RetrieveStockMarketInfoPort {

    private final AlphaVantageApi alphaVantageApi;

    @Override
    public StockMarketDataResponse getStockMarketInfo(String symbol, String apiKey) {
        var apiResponse = alphaVantageApi.retrieveStockMarketInfo(symbol, apiKey);
        var prices = apiResponse.series().entrySet()
                .stream()
                .map(e -> new Price(e.getKey(), e.getValue().openPrice(), e.getValue().highPrice(), e.getValue().lowPrice(), e.getValue().closePrice()))
                .toList();
        prices.stream().sorted(Comparator.comparing(Price::getLocalDate).reversed());
        var variationClosingValue = getVariationFromLastTwoClosingValues(List.of(prices.get(0), prices.get(1)));
        return new StockMarketDataResponse(apiResponse.metaData().symbol(), variationClosingValue, prices);
    }

    private String getVariationFromLastTwoClosingValues(List<Price> prices) {
        var lowestClosingValue = prices.stream().min(Comparator.comparing(Price::getClosePriceNumber)).get().getClosePriceNumber();
        var higClosingValue = prices.stream().max(Comparator.comparing(Price::getClosePriceNumber)).get().getClosePriceNumber();
        var variation = ((higClosingValue - lowestClosingValue) / higClosingValue) * 100;
        var decimalFormat = new DecimalFormat("0.00");
        return decimalFormat.format(variation);
    }
}
