package com.stockMarket.apis.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public record StockMarketInfo(
        @JsonProperty("Meta Data") MetaData metaData,
        @JsonProperty("Time Series (Daily)") Map<String, Price> series
        ) {
    public record MetaData(
            @JsonProperty("1. Information") String information,
            @JsonProperty("2. Symbol") String symbol,
            @JsonProperty("3. Last Refreshed") String date,
            @JsonProperty("4. Output Size") String outputSize,
            @JsonProperty("5. Time Zone") String timezone
    ) {}

    public record Price(
            @JsonProperty("1. open") String openPrice,
            @JsonProperty("2. high") String highPrice,
            @JsonProperty("3. low") String lowPrice,
            @JsonProperty("4. close") String closePrice,
            @JsonProperty("5. volume") String volume
    ) {}
}
