package com.stockMarket.apis;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stockMarket.apis.model.StockMarketInfo;
import com.stockMarket.configuration.ServiceProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

@Component
public class AlphaVantageApi extends BaseApi {

    public AlphaVantageApi(ServiceProperties serviceProperties, ApiClient apiClient, ObjectMapper mapper) {
        super(serviceProperties, apiClient, mapper);
    }

    public StockMarketInfo retrieveStockMarketInfo(String symbol, String apiKey) {
        var url = serviceProperties.getAlphaVantageProperties().get("stockPath").replace("{symbol}", symbol).replace("{apikey}", apiKey);
        return apiClient.getClient("alphaVantageUrl").get()
                .uri(url)
                .header("accept", "application/json")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<StockMarketInfo>() {})
                .block();
    }
}
