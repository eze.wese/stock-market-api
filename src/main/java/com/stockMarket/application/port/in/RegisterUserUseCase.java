package com.stockMarket.application.port.in;

import com.stockMarket.domain.user.UserData;

public interface RegisterUserUseCase {
    String registerUser(UserData userData);
}
