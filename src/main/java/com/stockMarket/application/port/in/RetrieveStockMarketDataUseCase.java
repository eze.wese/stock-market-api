package com.stockMarket.application.port.in;

import com.stockMarket.domain.stockMarketData.StockMarketDataResponse;

public interface RetrieveStockMarketDataUseCase {
    StockMarketDataResponse retrieveStockMarketData(String symbol, String apiKey);
}
