package com.stockMarket.application.port.out;

import com.stockMarket.domain.user.UserData;

public interface RegisterUserDataPort {
    void saveUserData(UserData userData);
}
