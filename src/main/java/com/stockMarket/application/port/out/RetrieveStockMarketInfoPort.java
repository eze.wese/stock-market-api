package com.stockMarket.application.port.out;

import com.stockMarket.domain.stockMarketData.StockMarketDataResponse;

public interface RetrieveStockMarketInfoPort {
    StockMarketDataResponse getStockMarketInfo(String symbol, String apiKey);
}
