package com.stockMarket.application.service;

import com.stockMarket.application.port.in.RegisterUserUseCase;
import com.stockMarket.application.port.out.RegisterUserDataPort;
import com.stockMarket.domain.user.UserData;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class RegisterUserService implements RegisterUserUseCase {

    private final RegisterUserDataPort registerUserDataPort;

    @Value("${service.alphaVantageProperties.apikey}")
    private String apiKey;

    @Override
    public String registerUser(UserData userData) {
        registerUserDataPort.saveUserData(userData);
        return apiKey;
    }
}
