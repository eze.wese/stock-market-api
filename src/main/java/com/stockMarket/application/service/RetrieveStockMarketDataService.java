package com.stockMarket.application.service;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.stockMarket.application.port.in.RetrieveStockMarketDataUseCase;
import com.stockMarket.application.port.out.RetrieveStockMarketInfoPort;
import com.stockMarket.domain.stockMarketData.StockMarketDataResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;

@RequiredArgsConstructor
@Component
public class RetrieveStockMarketDataService implements RetrieveStockMarketDataUseCase {

    private final RetrieveStockMarketInfoPort retrieveStockMarketInfoPort;
    private final Cache<String, StockMarketDataResponse> stockMarketDataResponseCache = Caffeine.newBuilder().expireAfterWrite(6, TimeUnit.HOURS).maximumSize(2).build();


    @Override
    public StockMarketDataResponse retrieveStockMarketData(String symbol, String apiKey) {
        var responseCached = stockMarketDataResponseCache.getIfPresent("StockMarketData");
        if (isNull(responseCached)) {
            var stockMarketData = retrieveStockMarketInfoPort.getStockMarketInfo(symbol, apiKey);
            stockMarketDataResponseCache.put("StockMarketData", stockMarketData);
            return stockMarketData;
        }
        return responseCached;
    }
}
