package com.stockMarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockMarketApplication {

	public static void main(String[] args) {
		ScopeUtils.calculateScopeSuffix();
		SpringApplication.run(StockMarketApplication.class, args);
	}

}
