package com.stockMarket.domain.user;

public record UserData(
        String name,
        String lastname,
        String email
) {
}
