package com.stockMarket.domain.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RegisterUserException extends ResponseStatusException {

    public RegisterUserException(HttpStatus httpStatus, String reason) {
        super(httpStatus, reason);
    }
}
