package com.stockMarket.domain;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class RestResponse {
    private int status;
    private String error;
    private String message;
    private String path;

    public static RestResponseBuilder builder() {
        return new RestResponseBuilder();
    }
}
