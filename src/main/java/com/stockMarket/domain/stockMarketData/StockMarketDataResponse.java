package com.stockMarket.domain.stockMarketData;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.List;

public record StockMarketDataResponse(
        String symbol,
        @JsonProperty("last_closing_price_variation") String lastClosingPriceVariation,
        List<Price> prices
) {
    public record Price(
            String date,
            @JsonProperty("open_price") String openPrice,
            @JsonProperty("high_price") String highPrice,
            @JsonProperty("low_price") String lowPrice,
            @JsonIgnore @JsonProperty("close_price") String closePrice
    ) {
        @JsonIgnore
        public Double getClosePriceNumber() {
            return Double.parseDouble(closePrice);
        }

        @JsonIgnore
        public LocalDate getLocalDate() { return LocalDate.parse(date); }
    }
}
