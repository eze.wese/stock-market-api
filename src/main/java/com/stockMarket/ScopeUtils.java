package com.stockMarket;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import static java.util.Objects.isNull;

public interface ScopeUtils {
    String SCOPE_SUFFIX = "SCOPE_SUFFIX";
    String SCOPE = "SCOPE";
    String SCOPE_VALUE = !isNull(System.getProperty(SCOPE)) ? System.getProperty(SCOPE) : System.getenv("SCOPE");

    static void calculateScopeSuffix() {
        String suffix = Optional.ofNullable(SCOPE_VALUE)
                .filter(StringUtils::isNoneBlank)
                .map(scope -> {
                    String[] tokens = scope.split("-");
                    return tokens[ tokens.length - 1 ];
                })
                .orElseThrow(() ->
                        new IllegalArgumentException("[Assertion Fail] SCOPE must be set to startup the application."));
        System.setProperty(SCOPE_SUFFIX, suffix);
    }
}