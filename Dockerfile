FROM gradle:7.4.1-jdk17 AS builder
COPY --chown=gradle:gradle . /application
WORKDIR /application
RUN gradle build --no-daemon && cp build/libs/*.jar application.jar && java -Djarmode=layertools -jar application.jar extract

FROM amazoncorretto:17-alpine
EXPOSE 8080
WORKDIR /application
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/application/ ./
ENTRYPOINT ["sh", "-c","java $JAVA_OPTS -Dreactor.netty.http.server.accessLogEnabled=true -Djava.security.egd=file:/dev/./urandom org.springframework.boot.loader.JarLauncher $@"]
