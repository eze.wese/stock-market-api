# Stock Market API

This REST API expose endpoint for signup user data and get apiKey. Also expose an endpoint to get stock market information for differents symbols like IBM, TSCO.LON (Tesco PLC), SHOP.TRT (Shopigy Inc), AAPL (Apple), MSFT (Microsoft), etc.

The REST API implementation was developed with [Hexagonal Architecture](https://blog.octo.com/hexagonal-architecture-three-principles-and-an-implementation-example).

## Requirements

1. Java 17.x.x
2. Gradle 7.4.2
3. MySQL

## MySQL Installation

- The installation of MySQL is a requirement to run the project correctly. So, in the root path of the project there is a docker compose [file](docker-compose.yaml) to install locally MySQL. For this at the project root path execute:
```bash
docker compose up -d
```

## Steps to Setup

**1. Clone the application**

```bash
git clone https://gitlab.com/eze.wese/stock-market-api.git
```

**2. Build and run the application app using gradle**

```bash
cd stock-rest-api
SPRING_PROFILES_ACTIVE=local gradle clean bootRun -DSCOPE=local
```

## Swagger

- Swagger is already configured in this project.
- The Swagger UI can be seen at http://localhost:8080/swagger-ui/index.html
- The Open API documentation v3 is at http://localhost:8080/v3/api-docs.